//
//  BaseViewModel.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol BaseViewModelProtocol : class {
    func processResponseData<T : Codable>(statusCode : Int, data: Data, objectConvert: T.Type) -> (errorMessage: String?, responseData: T?)
}

class BaseViewModel : BaseViewModelProtocol {
    public let disposeBag = DisposeBag()
    
    func processResponseData<T : Codable>(statusCode : Int, data: Data, objectConvert: T.Type) -> (errorMessage: String?, responseData: T?){
        if String(statusCode).hasPrefix("2") {  //success status code : 2xx
            //success
            let decoder = JSONDecoder()
            do{
                let responseData = try decoder.decode(objectConvert, from: data)
                return (nil, responseData)
            }
            catch let error {
                print(error)
                let errorMessage = TextGlobal.MESSAGE_ERROR_PARSE_JSON
                return (errorMessage, nil)
            }
        } else {
            //fail
            let errorMessage = self.getMessageError(data: data)
            return (errorMessage, nil)
        }
    }
    
    private func getMessageError(data: Data) -> String {
        var errorMessage = "error"
        let decoder = JSONDecoder()
        do{
            let errorObj = try decoder.decode(ErrorObject.self, from: data)
            //get messages in array errors
            if let errors = errorObj.errors, errors.count > 0 {
                let error = errors[0]
                errorMessage = error.message ?? "error"
            } else
                if let errMessage = errorObj.message, !errMessage.isEmpty {
                    errorMessage = errMessage
            }
            return errorMessage
            
        }catch let error {
            print(error)
            errorMessage = TextGlobal.MESSAGE_ERROR_PARSE_JSON
            return errorMessage
        }
    }
    
}
