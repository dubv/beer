//
//  BeerViewModel.swift
//  BEERme
//
//  Created by Gone on 5/8/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BeerViewModel: BaseViewModel {
    public var isLoading = PublishSubject<Bool>()
    public var onSuccess = BehaviorRelay<[[BeerObject]]>(value: [[]])
    public var onFailure = PublishSubject<String>()
    
    public func requestListBeer() {
        isLoading.onNext(true)
        RxAPIManager.shared.request(url: AppUrl.getListBeer(), method: .get, param: nil, header: nil, taskId: nil).asObservable().subscribe { (result) in
            guard let result = result.element else { return }
            switch result {
            case .success(let statusCode, let responseData):
                let response = self.processResponseData(statusCode: statusCode, data: responseData, objectConvert: ListBeerResponse.self)
                if let errorMessage = response.errorMessage, !errorMessage.isEmpty {
                    self.onFailure.onNext(errorMessage)
                    self.isLoading.onNext(false)
                } else {
                    let groupedDictionary = Dictionary(grouping: (response.responseData?.data)!) { (beer) -> Character in
                        return beer.name!.first!
                    }
                    var listBeerGrouped = [[BeerObject]]()
                    let key = groupedDictionary.keys.sorted()
                    key.forEach { (item) in
                        listBeerGrouped.append(groupedDictionary[item]!)
                    }
                    self.onSuccess.accept(listBeerGrouped)
                    self.isLoading.onNext(false)
                }
            case .failure(let error):
                self.onFailure.onNext(error.localizedDescription)
                self.isLoading.onNext(false)
            }
        }.disposed(by: disposeBag)
    }
}
