//
//  UserSubmitionModel.swift
//  BEERme
//
//  Created by DU on 5/12/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class UserSubmitionModel: BaseViewModel {
    public var isLoading = PublishSubject<Bool>()
    public var onSuccess = PublishSubject<String>()
    public var onFailure   = PublishSubject<String>()
    public var code = BehaviorRelay<String>(value: "")
    public var security = BehaviorRelay<String>(value: "")
    
    public func isValidate() -> Bool {
        let codeText = code.value
        guard codeText.count > 0 else {
            onFailure.onNext("code invalid!")
            return false
        }
        
        let securityText = security.value
        guard securityText.count > 0 else {
            onFailure.onNext("security invalid!")
            return false
        }
        return true
    }
    
    public func requestSubmit() {
        let submitRequest = SubmitRequest(code: code.value, security: security.value)
        let submitData = try? JSONEncoder().encode(submitRequest)
        self.isLoading.onNext(true)
        
        RxAPIManager.shared.request(url: AppUrl.doSubmition(), method: .post, param: submitData, header: nil, taskId: nil).asObservable().subscribe { (result) in
            guard let result = result.element else { return }
            switch result {
            case .success(let statusCode, let responseData):
                let response = self.processResponseData(statusCode: statusCode, data: responseData, objectConvert: SubmitResponse.self)
                if let errorMessage = response.errorMessage, !errorMessage.isEmpty {
                    self.onFailure.onNext(errorMessage)
                    self.isLoading.onNext(false)
                } else {
                    self.onSuccess.onNext(self.getSubmitResponse(submition: response.responseData))
                    self.isLoading.onNext(false)
                }
            case .failure(let error):
                self.onFailure.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    private func getSubmitResponse(submition: SubmitResponse?) -> String {
        guard let submit = submition, let code = submit.code else {
            return "-----"
        }
        return code
    }
}
