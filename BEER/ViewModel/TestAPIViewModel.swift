//
//  TestViewModel.swift
//  BEER
//
//  Created by Gone on 4/23/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class TestAPIViewModel: BaseViewModel {
    public var isLoading = PublishSubject<Bool>()
    public var loadTicketSuccess = BehaviorRelay<[TicketObject]>(value: [])
    public var loadTicketFailure = PublishSubject<String>()
    public var loadNotifiSuccess = PublishSubject<[NotificationElement]>()
    public var loadNotifiFailure = PublishSubject<String>()
    
    public func requestListTickets() {
        APIManager.shared.request(url: AppUrl.getListTicket(), method: .get, param: nil, header: nil, taskId: nil) { [weak self] (result) in
            guard let `self` = self else { return }
            switch result {
            case .success(let statusCode, let responseData):
                let response = self.processResponseData(statusCode: statusCode, data: responseData, objectConvert: TicketObject.self)
                if let errorMessage = response.errorMessage, !errorMessage.isEmpty {
                    self.loadTicketFailure.onNext(errorMessage)
                } else {
                    if let ticket = response.responseData {
                        self.loadTicketSuccess.accept([ticket])
                    } else {
                        self.loadTicketSuccess.accept([])
                    }
                }
            case .failure(let error):
                self.loadTicketFailure.onNext(error.localizedDescription)
            }
        }
    }
    
    public func requestListNotifications(pageNumber: Int, sizeNumber: Int) {
        self.isLoading.onNext(true)
        APIManager.shared.request(url: AppUrl.getListNotifi(pageNumber: pageNumber, pageSize: sizeNumber), method: .get, param: nil, header: nil, taskId: nil) { [weak self] (result) in
            guard let `self` = self else { return }
            self.isLoading.onNext(false)
            switch result {
            case .success(let statusCode, let responseData):
                let response = self.processResponseData(statusCode: statusCode, data: responseData, objectConvert: [NotificationElement].self)
                if let errorMessage = response.errorMessage, !errorMessage.isEmpty {
                    self.loadNotifiFailure.onNext(errorMessage)
                } else {
                    if let notifi = response.responseData {
                        self.loadNotifiSuccess.onNext(notifi)
                    }
                }
            case .failure(let error):
                self.loadNotifiFailure.onNext(error.localizedDescription)
            }
        }
    }
}
