//
//  CountdownView.swift
//  BEER
//
//  Created by DU on 4/26/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

struct Countdown {
    var secondFirstNumber, secondSecondNumber: Int?
    var minuteFirstNumber, minuteSecondNumber: Int?
    var hourFirstNumber, hourSecondNumber: Int?
    var textColor: UIColor?
    var largerFont: UIFont?
    var smallFont: UIFont?
}

class CountdownView: UIView {
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var secondFirstNumberLabel: UILabel!
    @IBOutlet private weak var secondSecondNumberLabel: UILabel!
    @IBOutlet private weak var smallSeperatorLabel: UILabel!
    @IBOutlet private weak var minuteFirstNumberLabel: UILabel!
    @IBOutlet private weak var minuteSecondNumberLabel: UILabel!
    @IBOutlet private weak var hourFirstNumberLabel: UILabel!
    @IBOutlet private weak var hourSecondNumberLabel: UILabel!
    @IBOutlet private weak var largerSeperatorLabel: UILabel!
    
    public var heightOfView: CGFloat? {
        didSet {
               configContentView()
        }
    }
    
    public var countdown: Countdown? {
        didSet {
            configCountdownView()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    public func configCountdownView() {
        secondFirstNumberLabel.text = countdown?.secondFirstNumber?.stringWithLeadingZeros
        secondSecondNumberLabel.text = countdown?.secondSecondNumber?.stringWithLeadingZeros
        minuteFirstNumberLabel.text = countdown?.minuteFirstNumber?.stringWithLeadingZeros
        minuteSecondNumberLabel.text = countdown?.minuteSecondNumber?.stringWithLeadingZeros
        hourFirstNumberLabel.text = countdown?.hourFirstNumber?.stringWithLeadingZeros
        hourSecondNumberLabel.text = countdown?.hourSecondNumber?.stringWithLeadingZeros
    }
    
    private func configContentView() {
        self.layoutIfNeeded()
        if let heightOfView = heightOfView {
            contentView.widthAnchor.constraint(equalToConstant: heightOfView/0.296).isActive = true
            configFontSize(fontHeight: heightOfView)
        } else {
            contentView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
            configFontSize(fontHeight: self.frame.height)
        }
    }
    
    private func configFontSize(fontHeight: CGFloat) {
        let largerLabel: [UILabel] = [hourSecondNumberLabel, hourFirstNumberLabel, largerSeperatorLabel, minuteSecondNumberLabel, minuteFirstNumberLabel]
        let smallLabel: [UILabel] = [secondFirstNumberLabel, secondSecondNumberLabel, smallSeperatorLabel]
        largerLabel.forEach { label in
            label.font = UIFont.systemFont(ofSize: fontHeight*0.9, weight: .bold)
        }
        smallLabel.forEach { label in
            label.font = UIFont.systemFont(ofSize: fontHeight/2.0, weight: .bold)
        }
    }
    
    private func initView() {
        Bundle.main.loadNibNamed("CountdownView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        contentView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    }
}
