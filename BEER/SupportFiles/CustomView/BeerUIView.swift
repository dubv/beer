//
//  BeerUIView.swift
//  BEERme
//
//  Created by DU on 5/12/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

@IBDesignable class BeerUIView: UIView {

    @IBInspectable var viewCornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = viewCornerRadius
        }
    }
}
