//
//  PageTabView.swift
//  BEERme
//
//  Created by DU on 5/16/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

protocol PageTabViewDelegate: NSObjectProtocol {
    func pageTabDidSelectItemAt(pageTab: PageTabView, index: Int)
}

protocol PageTabViewDataSource: NSObjectProtocol {
    func pageTabViewDataSource(pageTabView: PageTabView) -> [String]
}

class PageTabView: UIView {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet public weak var pageTabCollectionView: UICollectionView!
    
    // MARK: - Properties
    weak var delegate: PageTabViewDelegate?
    weak var dataSource: PageTabViewDataSource?
    private var horizontalView = UIView()
    private var sectionInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    private var currentIndexPath: IndexPath?
    private var infiniteDataSource: Int?
    
    public var selectionPerItem: CGFloat = 4 {
        didSet {
            self.reloadData()
        }
    }
    
    public var isSizeToFitCellsNeeded: Bool = false {
        didSet{
            self.reloadData()
        }
    }
    
    public var indexPathSelected: IndexPath = IndexPath(item: 0, section: 0) {
        didSet {
            self.pageTabCollectionView.selectItem(at: indexPathSelected, animated: true, scrollPosition: .centeredHorizontally)
            updateHorizontalView(indexPath: indexPathSelected)
        }
    }

    // MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("PageTabView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.anchor(top: self.topAnchor, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: self.frame.width, height: self.frame.height)
        registerCell()
        setupCollectionView()
        setupHorizontalView()
    }
    
    // MARK: - Private Method
    private func registerCell() {
        pageTabCollectionView.register(PageTabCVCell.self)
    }
    
    private func setupCollectionView() {
        pageTabCollectionView.delegate = self
        pageTabCollectionView.dataSource = self
        
        if let collectionviewFlowLayout = pageTabCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionviewFlowLayout.estimatedItemSize = CGSize(width: self.contentView.frame.width/selectionPerItem, height: self.contentView.frame.height)
            collectionviewFlowLayout.minimumInteritemSpacing = sectionInsets.left
            collectionviewFlowLayout.minimumLineSpacing = sectionInsets.left
            collectionviewFlowLayout.scrollDirection = .horizontal
        }
    }
    
    private func updateHorizontalView(indexPath: IndexPath) {
        if let cell = pageTabCollectionView.cellForItem(at: indexPath) as? PageTabCVCell {
            UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.horizontalView.frame = CGRect(x: cell.frame.minX, y: self.pageTabCollectionView.frame.maxY-self.contentView.frame.height*0.1, width: cell.contentView.frame.width, height: self.contentView.frame.height*0.1)
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    private func setupHorizontalView() {
        horizontalView.frame = CGRect(x: sectionInsets.left, y: pageTabCollectionView.frame.maxY-self.contentView.frame.height*0.1, width: isSizeToFitCellsNeeded ? setupWidthCell(indexPath: indexPathSelected) : self.contentView.frame.width/selectionPerItem, height: self.contentView.frame.height*0.1)
        pageTabCollectionView.addSubview(horizontalView)
        horizontalView.backgroundColor = AppColor.selectionSegmentColor
    }
    
    private func setupWidthCell(indexPath: IndexPath) -> CGFloat {
        guard let cell = pageTabCollectionView.cellForItem(at: indexPath) as? PageTabCVCell else { return self.frame.width/selectionPerItem }
        return cell.contentView.frame.width
    }
    
    private func getSelectionValues() -> [String] {
        guard let selectionValues = self.dataSource?.pageTabViewDataSource(pageTabView: self) else { return [] }
        return selectionValues
    }
    
    // MARK: - Public Method
    public func reloadData() {
        pageTabCollectionView.reloadData()
        contentView.isHidden = !(getSelectionValues().count > 0)
    }
}
// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension PageTabView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getSelectionValues().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageTabCVCell.indentifier, for: indexPath) as? PageTabCVCell else { return UICollectionViewCell() }
        cell.selectionTitle = self.getSelectionValues()[indexPath.row]
        
        /// Keep highlight selectionTitle
        if let currentIndexPath = self.currentIndexPath, currentIndexPath == indexPath {
            cell.isSelected = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.pageTabDidSelectItemAt(pageTab: self, index: indexPath.row)
        self.updateHorizontalView(indexPath: indexPath)
        self.currentIndexPath = indexPath
    }
}
