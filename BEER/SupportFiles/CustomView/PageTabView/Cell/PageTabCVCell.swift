//
//  PageTabCVCell.swift
//  BEERme
//
//  Created by DU on 5/14/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class PageTabCVCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet public weak var tabTitleLabel: UILabel!
    
    // MARK: - Properties
    private var horizonTabBarLeadingConstraint: NSLayoutConstraint!
    private var indicatorView: UIView!
    public var selectionTitle: String = "" {
        didSet {
            reloadUI()
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }

    
    override var isSelected: Bool {
        didSet {
            self.tabTitleLabel.textColor = self.isSelected ? AppColor.selectionSegmentColor : AppColor.textMainColor
        }
    }
 
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        // Specify you want full width
        let targetSize = CGSize(width: 0, height: layoutAttributes.frame.height)
        // Calculate the size (width) using Auto Layout
        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .defaultLow, verticalFittingPriority: .required)
        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
        
        // Assign the new size to the layout attributes
        autoLayoutAttributes.frame = autoLayoutFrame
        return autoLayoutAttributes
    }
    
    private func reloadUI() {
        tabTitleLabel.text = selectionTitle
    }
}
