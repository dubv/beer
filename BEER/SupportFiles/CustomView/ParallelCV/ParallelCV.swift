//
//  ParallelCV.swift
//  BEERme
//
//  Created by DU on 5/14/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

enum SelectionType: String, CaseIterable {
    case onion = "#たまねぎ"
    case apple = "#りんご"
    case yuzu = "#ゆず"
    case popular = "#世俗"
    case fig = "#いちじく"
    case cheese = "#チーズ"
}

class ParallelCV: UIView {
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var containPageTabView: UIView!
    @IBOutlet private weak var containPageViewController: UIView!
    
    // MARK: - Properties
    private var pageViewController = UIPageViewController()
    private var pageTabView = PageTabView()
    private var selectionArray = [String]()
    private var currentIndex: Int = 0
    private var sectionInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    private let disposeBag = DisposeBag()
    
    // MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    // MARK: - Private Method
    private func initView() {
        Bundle.main.loadNibNamed("ParallelCV", owner: self, options: nil)
        self.addSubview(contentView)
        if #available(iOS 11.0, *) { setupContentView(constrain: self.safeAreaLayoutGuide.topAnchor)
        } else {
            setupContentView(constrain: self.topAnchor)
        }
        setupData()
        setupCollectionView()
        setupPageVC()
    }
    
    private func setupContentView(constrain: NSLayoutYAxisAnchor) {
        contentView.anchor(top: constrain, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: self.frame.width, height: self.frame.height)
    }
    
    private func setupCollectionView() {
        containPageTabView.addSubview(pageTabView)
        pageTabView.anchor(top: containPageTabView.topAnchor, paddingTop: 0, bottom: containPageTabView.bottomAnchor, paddingBottom: 0, left: containPageTabView.leftAnchor, paddingLeft: 0, right: containPageTabView.rightAnchor, paddingRight: 0, width: containPageTabView.frame.width, height: containPageTabView.frame.height)
        pageTabView.delegate = self
        pageTabView.dataSource = self
        pageTabView.isSizeToFitCellsNeeded = true
        pageTabView.indexPathSelected = IndexPath(item: 0, section: 0)
    }
    
    private func setupData() {
        self.selectionArray.removeAll()
        SelectionType.allCases.forEach { type in
            self.selectionArray.append(type.rawValue)
        }
    }
    
    private func setupPageVC() {
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: containPageViewController.frame.width, height: containPageViewController.frame.height)
        containPageViewController.addSubview(pageViewController.view)
        pageViewController.setViewControllers([viewControllerAtIndex(self.currentIndex)], direction: .forward, animated: false, completion: nil)
        pageViewController.view.backgroundColor = .white
        pageViewController.delegate = self
        pageViewController.dataSource = self
    }
    
    private func viewControllerAtIndex(_ pageIndex: Int) -> ParallelPageVC {
        let parallelPageVC = ParallelPageVC()
        parallelPageVC.dataSource = self
        parallelPageVC.fetchValue(pageIndex, selectionArray)
        parallelPageVC.didSelected = { [weak self] () in
            guard let `self` = self else { return }
            self.currentIndex = parallelPageVC.pageIndex
        }
        return parallelPageVC
    }
}
// MARK: - UIPageViewControllerDelegate, UIPageViewControllerDataSource
extension ParallelCV: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var pageIndex = (viewController as? ParallelPageVC)?.pageIndex else { return ParallelPageVC() }
        if pageIndex == 0 {
            return nil
        }
        pageIndex -= 1
        return viewControllerAtIndex(pageIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard var pageIndex = (viewController as? ParallelPageVC)?.pageIndex else { return ParallelPageVC() }
        pageIndex += 1
        if pageIndex == selectionArray.count {
            return nil
        }
        return viewControllerAtIndex(pageIndex)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return selectionArray.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.pageTabView.indexPathSelected = IndexPath(item: self.currentIndex, section: 0)
    }
}

// MARK: - PageTabViewDelegate
extension ParallelCV: PageTabViewDelegate, PageTabViewDataSource, ParallelPageViewDataSource {
    func selectionValue(selectionText: String) {
        self.selectionArray.append(selectionText)
        self.pageTabView.reloadData()
        self.pageViewController.setViewControllers([viewControllerAtIndex(self.currentIndex)], direction: .forward, animated: false, completion: nil)
    }
    
    func deleteSelectionValueAt(indexPath: IndexPath) {
        self.selectionArray.remove(at: indexPath.row)
        self.pageTabView.reloadData()
        self.pageViewController.setViewControllers([viewControllerAtIndex(self.currentIndex)], direction: .forward, animated: false, completion: nil)
    }
    
    func sortSelectionValueFrom(sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let rowToMove = self.selectionArray[sourceIndexPath.row]
        self.selectionArray.remove(at: sourceIndexPath.row)
        self.selectionArray.insert(rowToMove, at: destinationIndexPath.row)
        self.pageTabView.reloadData()
        self.pageViewController.setViewControllers([viewControllerAtIndex(self.currentIndex)], direction: .forward, animated: false, completion: nil)
    }
    
    func pageTabViewDataSource(pageTabView: PageTabView) -> [String] {
        return selectionArray
    }
    
    func pageTabDidSelectItemAt(pageTab: PageTabView, index: Int) {
        self.pageTabView.pageTabCollectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
        if index != self.currentIndex {
             self.pageViewController.setViewControllers([viewControllerAtIndex(index)], direction: (index > self.currentIndex) ? .forward : .reverse, animated: true, completion: nil)
        }
        self.currentIndex = index
    }
}
