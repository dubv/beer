//
//  AddNewSelectionView.swift
//  BEERme
//
//  Created by DU on 5/17/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

protocol AddNewSelectionViewDataSource: NSObjectProtocol {
    func setSelectionValue(value: String)
}

class AddNewSelectionView: UIView {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var addedTextField: UITextField!
    @IBOutlet private weak var addedButton: UIButton!
    
    // MARK: - Properties
    private var selectionString: String?
    weak var dataSource: AddNewSelectionViewDataSource?
    
    // MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("AddNewSelectionView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.anchor(top: self.topAnchor, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: self.frame.width, height: self.frame.height)
        
        setupTextField()
        setupUI()
    }
    
    // MARK: - Private Method
    private func setupUI() {
        addedTextField.addTarget(selectionString, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    private func reloadData() {
        addedTextField.text = selectionString
    }
    
    private func setupTextField() {
        addedTextField.delegate = self
    }
    
    // MARK: - Public Method
    
    // MARK: - Target
    @objc private func textFieldDidChange(_ textField: UITextField) {
        selectionString = textField.text
    }
    
    // MARK: - IBAction
    @IBAction private func touchUpInsideAdded(_ sender: UIButton) {
        guard let value = self.selectionString else { return }
        dataSource?.setSelectionValue(value: value)
        self.endEditing(true)
    }
}
// MARK: - UITextFieldDelegate
extension AddNewSelectionView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        reloadData()
    }
}
