//
//  ParallelPageVC.swift
//  BEERme
//
//  Created by DU on 5/16/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

protocol ParallelPageViewDataSource: NSObjectProtocol {
    func selectionValue(selectionText: String)
    func deleteSelectionValueAt(indexPath: IndexPath)
    func sortSelectionValueFrom(sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
}

class ParallelPageVC: BaseVC {

    //MARK: - IBOutlet
    @IBOutlet private weak var selectionTitleLabel: UILabel!
    @IBOutlet private weak var formView: UIView!
    
    //MARK: - Properties
    weak var dataSource: ParallelPageViewDataSource?
    private var addNewSelectionView = AddNewSelectionView()
    private var changeSelectionView = ChangeSelectionView()
    public var pageIndex: Int = 0
    public var didSelected: (() -> Void)?
    public var selectionTitle: String?
    public var selectionArray = [String]()
    private var addTextFieldString: String?
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        didSelected?()
        updateUI()
    }
    
    //MARK: - Initialization Method
    private func setupUI() {
        selectionTitleLabel.text = selectionTitle
    }
    
    private func setupData() {
        
    }
  
    //MARK: - Private Method
    private func updateUI() {
        switch pageIndex {
        case 0:
            addNewSelectionView.dataSource = self
            changeSelectionView.removeFromSuperview()
            implementView(subView: addNewSelectionView)
        case 1:
            break
        case 3:
            changeSelectionView.dataSource = self
            addNewSelectionView.removeFromSuperview()
            implementView(subView: changeSelectionView)
        default:
            break
        }
    }
    
    private func implementView(subView: UIView) {
        formView.addSubview(subView)
        subView.anchor(top: formView.topAnchor, paddingTop: 0, bottom: formView.bottomAnchor, paddingBottom: 20, left: formView.leftAnchor, paddingLeft: 0, right: formView.rightAnchor, paddingRight: 0, width: formView.frame.width, height: formView.frame.height)
        self.view.layoutIfNeeded()
    }
    
    private func reloadUI() {
        
    }
    
    //MARK: - Public Method
    public func fetchValue(_ pageIndex: Int, _ data: [String]) {
        self.pageIndex = pageIndex
        self.selectionTitle = data[pageIndex]
        self.selectionArray = data
    }
    
    func updateData() {
        
    }
    
    // MARK: - Target
    
    //MARK: - IBAction
}
// MARK: - ParallelPageViewDataSource
extension ParallelPageVC: AddNewSelectionViewDataSource, ChangeSelectionViewDataSource {
    func setSelectionValue(value: String) {
        dataSource?.selectionValue(selectionText: value)
    }

    func deleteSelectionValueAt(indexPath: IndexPath) {
        dataSource?.deleteSelectionValueAt(indexPath: indexPath)
    }
    
    func sortSelectionValueFrom(sourceIndexPath: IndexPath, to indexPath: IndexPath) {
        dataSource?.sortSelectionValueFrom(sourceIndexPath: sourceIndexPath, to: indexPath)
    }
    
    func selectionValues(changeSelectionView: ChangeSelectionView) -> [String] {
        return selectionArray
    }
}
