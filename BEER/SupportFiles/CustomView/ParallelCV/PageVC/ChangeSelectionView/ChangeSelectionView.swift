//
//  ChangeSelectionView.swift
//  BEERme
//
//  Created by DU on 5/17/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

protocol ChangeSelectionViewDataSource: NSObjectProtocol {
    func selectionValues(changeSelectionView: ChangeSelectionView) -> [String]
    func deleteSelectionValueAt(indexPath: IndexPath)
    func sortSelectionValueFrom(sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
}

class ChangeSelectionView: UIView {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var changeSelectionTableView: UITableView!
    
    // MARK: - Properties
    weak var dataSource: ChangeSelectionViewDataSource?

    // MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("ChangeSelectionView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.anchor(top: self.topAnchor, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: self.frame.width, height: self.frame.height)
        registerCell()
        setupTableView()
    }
    
    // MARK: - Private Method
    private func registerCell() {
        changeSelectionTableView.register(ChangeSelectionCell.self)
    }
    
    private func setupTableView() {
        changeSelectionTableView.delegate = self
        changeSelectionTableView.dataSource = self
        changeSelectionTableView.isEditing = true
    }
    
    private func getSelectionValues() -> [String] {
        guard let selectionValues = dataSource?.selectionValues(changeSelectionView: self) else { return [] }
        return selectionValues
    }
    
    // MARK: - Public Method
    public func reloadData() {
        changeSelectionTableView.reloadData()
        contentView.isHidden = !(getSelectionValues().count > 0)
    }
    
    // MARK: - Target
    // MARK: - IBAction
}
// MARK: - UITableViewDelegate, UITableViewDataSource
extension ChangeSelectionView: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getSelectionValues().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ChangeSelectionCell.self, for: indexPath)
        cell.selectionString = getSelectionValues()[indexPath.row]
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            dataSource?.deleteSelectionValueAt(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        dataSource?.sortSelectionValueFrom(sourceIndexPath: sourceIndexPath, to: destinationIndexPath)
    }
}
