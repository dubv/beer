//
//  ChangeSelectionCell.swift
//  BEERme
//
//  Created by DU on 5/17/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class ChangeSelectionCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var selectionTitleLabel: UILabel!
    
    // MARK: - Properties
    public var selectionString: String = "" {
        didSet {
            selectionTitleLabel.text = selectionString
        }
    }
    
    // MARK: - Initialization Method
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    
    // MARK: - IBAction
}
