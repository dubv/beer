//
//  SITextView.swift
//  Ataru
//
//  Created by Nguyen Quang Manh on 4/5/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class BeerTextView: UITextView {
    
//    @IBInspectable var placeholder: String? {
//        didSet {
//            self.updatePlaceHolder()
//        }
//    }
    
    @IBInspectable var fontStyle: UIFont = UIFont.systemFont(ofSize: 13) {
        didSet {
            self.setupFontStyle()
        }
    }
    
    @IBInspectable var viewCornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = viewCornerRadius
        }
    }
    
    var bGColor: UIColor = AppColor.backgroundColor {
        didSet {
            self.backgroundColor = bGColor
        }
    }
    
    lazy var placeHolderLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    override var text: String! {
        didSet {
            self.updatePlaceHolder()
        }
    }
    
    // MARK: - Initialization Method
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidChangeNotification, object: nil)
    }
    
    // MARK: - Private Method
    private func initView() {
        // setup place holder
        setupPlaceHolder()
    }
    
    private func setupPlaceHolder() {
//        placeHolderLabel.text = placeholder ?? ""
        placeHolderLabel.numberOfLines = 0
        placeHolderLabel.lineBreakMode = .byWordWrapping
        placeHolderLabel.textColor = UIColor.lightGray
        placeHolderLabel.isHidden = self.text.count > 0
        
        setupFontStyle()
        self.addSubview(placeHolderLabel)
        self.resizePlaceholder()
        
        // Detect when text change
        NotificationCenter.default.addObserver(self, selector: #selector(updatePlaceHolder), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    private func setupFontStyle() {
        placeHolderLabel.font = fontStyle
        self.font = fontStyle
    }
    
    private func resizePlaceholder() {
        placeHolderLabel.anchor(top: self.topAnchor, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: -10, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    @objc private func updatePlaceHolder() {
//        placeHolderLabel.text = placeholder
        self.resizePlaceholder()
        placeHolderLabel.isHidden = !self.text.isEmpty
    }
}
