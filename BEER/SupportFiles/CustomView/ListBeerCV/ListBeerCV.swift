
//
//  ListBeerCV.swift
//  BEERme
//
//  Created by DU on 5/10/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

enum ModesView: Int, CaseIterable {
    case verticalSplit = 1
    case viewCarousel
    case viewColumn
}

struct DataSource<Element> {
    var items = [Element]()
    mutating func initValue(_ item: [Element]) {
        items = item    }
}

class ListBeerCV: UIView {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet weak var beerCollectionView: UICollectionView!
    @IBOutlet private weak var containModeView: UIView!
    @IBOutlet private weak var heightModeViewConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private var modeView = ModeView()
    private var listModeViewCVCell = [UICollectionViewCell]()
    private var modeViewType = ModesView.verticalSplit
    private var itemPerRow: CGFloat = 3.0
    private var sectionInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16)
    private var tranformsTimer: Timer!
    private var defaultHeightModeView: CGFloat = 0
    var dataSource: [[BeerObject]] = [[BeerObject]]() {
        didSet {
            beerCollectionView.reloadData()
        }
    }
    
    // MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("ListBeerCV", owner: self, options: nil)
        self.addSubview(contentView)
        if #available(iOS 11.0, *) { setupContentView(constrain: self.safeAreaLayoutGuide.topAnchor)
        } else {
            setupContentView(constrain: self.topAnchor)
        }
        
        registerCell()
        setupCVFlowLayout()
        setupModeView()
    }
    
    private func setupContentView(constrain: NSLayoutYAxisAnchor) {
         contentView.anchor(top: constrain, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: self.frame.width, height: self.frame.height)
    }
    
    private func setupModeView() {
        containModeView.addSubview(modeView)
        modeView.anchor(top: containModeView.topAnchor, paddingTop: 0, bottom: containModeView.bottomAnchor, paddingBottom: 0, left: containModeView.leftAnchor, paddingLeft: 0, right: containModeView.rightAnchor, paddingRight: 0, width: containModeView.frame.width, height: containModeView.frame.height)
        modeView.modeViewChangeDelegate = self
        defaultHeightModeView = heightModeViewConstraint.constant
        containModeView.shadow()
    }
    
    private func handleModeViewHidden(trigger: Bool) {
        UIView.animate(withDuration: 1) {
            self.containModeView.isHidden = trigger
            self.heightModeViewConstraint.constant = trigger ? 0 : self.defaultHeightModeView
        }
    }
    
    private func setupCVFlowLayout() {
        beerCollectionView.delegate = self
        beerCollectionView.dataSource = self
        if let collectionFlowLayout = beerCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionFlowLayout.sectionHeadersPinToVisibleBounds = true
            let availableWith = beerCollectionView.frame.width
            switch modeViewType {
            case .verticalSplit:
                let paddingSpace = sectionInsets.left * (itemPerRow+1)
                let widthPerItem = (availableWith-paddingSpace) / itemPerRow
                collectionFlowLayout.estimatedItemSize = CGSize(width: widthPerItem, height: widthPerItem)
            case .viewCarousel:
                collectionFlowLayout.estimatedItemSize = CGSize(width: availableWith-(sectionInsets.left*((itemPerRow-1))), height: availableWith*0.5)
            case .viewColumn:
                collectionFlowLayout.estimatedItemSize = CGSize(width: availableWith-(sectionInsets.left*((itemPerRow-1))), height: availableWith)
            }
            beerCollectionView.alwaysBounceVertical = true
        }
    }
    
    // MARK: - Private Method
    private func registerCell() {
        beerCollectionView.register(VerticalSplitCVCell.self)
        beerCollectionView.register(CarouselCVCell.self)
        beerCollectionView.register(ColumnCVCell.self)
        beerCollectionView.register(HeaderSectionCVCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderSectionCVCell.indentifier)
        beerCollectionView.register(FlashCollectionViewCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: FlashCollectionViewCell.indentifier)
    }
    
    // MARK: - Private Method
    private func modeTranformsViewCV() {
        self.beerCollectionView.alpha = 0.5
        UIView.transition(with: beerCollectionView, duration: 0.5, options: .showHideTransitionViews, animations: {
            self.beerCollectionView.alpha = 1
        }, completion: nil)
            tranformsTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { _ in
            self.showProgress(false)
        }
        setupCVFlowLayout()
        self.beerCollectionView.reloadData()
    }

    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
}
// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewFlowLayout
extension ListBeerCV: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 2 ? 0 : dataSource[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch modeViewType {
        case .verticalSplit:
            let item = collectionView.dequeue(VerticalSplitCVCell.self, forIndexPath: indexPath)
            return item
        case .viewCarousel:
            let item = collectionView.dequeue(CarouselCVCell.self, forIndexPath: indexPath)
            item.beerObject = dataSource[indexPath.section][indexPath.row]
            return item
        case .viewColumn:
            let item = collectionView.dequeue(ColumnCVCell.self, forIndexPath: indexPath)
            item.beerObject = dataSource[indexPath.section][indexPath.row]
            return item
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 2 {
            return CGSize(width: self.frame.width, height: 150)
        } else {
            return CGSize(width: self.frame.width, height: 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableview = UICollectionReusableView()
        if kind == UICollectionView.elementKindSectionHeader {
            if indexPath.section == 2 {
                 guard let flashSectionView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: FlashCollectionViewCell.indentifier, for: indexPath) as? FlashCollectionViewCell else { return UICollectionReusableView() }
                reusableview = flashSectionView
            } else {
                 guard let headerSectionView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderSectionCVCell.indentifier, for: indexPath) as? HeaderSectionCVCell else { return UICollectionReusableView() }
                if let headerObject = dataSource[indexPath.section].first, let headerTitle = headerObject.name?.first {
                    headerSectionView.titleHeader = headerTitle.description
                }
                reusableview = headerSectionView
            }
        }
        return reusableview
    }
}
// MARK: - CollectionUtilitiesVC
extension ListBeerCV: ModeViewChange {
    func modeViewChanged(mode: Int) {
        showProgress(true)
        guard let value = ModesView(rawValue: mode) else {
            return
        }
        switch value {
        case .verticalSplit:
            modeViewType = .verticalSplit
            modeTranformsViewCV()
        case .viewCarousel:
            modeViewType = .viewCarousel
            modeTranformsViewCV()
        case .viewColumn:
            modeViewType = .viewColumn
            modeTranformsViewCV()
        }
    }
}
