//
//  FlashCollectionViewCell.swift
//  BEERme
//
//  Created by DU on 5/11/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class FlashCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var flashCollectionView: UICollectionView!
    
    // MARK: - Properties
    private var sectionInsets = UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16)
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Initialization Method
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCollectionViewFlow()
        registerCell()
    }
    
    // MARK: - Private Method
    private func setupCollectionViewFlow() {
        flashCollectionView.delegate = self
        flashCollectionView.dataSource = self
        if let flashCVFlowLayout = flashCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flashCVFlowLayout.itemSize = CGSize(width: self.contentView.frame.height, height: self.contentView.frame.height)
            flashCVFlowLayout.scrollDirection = .horizontal
        }
    }
    
    private func registerCell() {
        flashCollectionView.register(FlashCVCell.self)
    }
    
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
}
// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension FlashCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeue(FlashCVCell.self, forIndexPath: indexPath)
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
