//
//  HeaderSectionCVCell.swift
//  BEERme
//
//  Created by DU on 5/10/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class HeaderSectionCVCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var titleHeaderLabel: UILabel!
    
    // MARK: - Properties
    var titleHeader: String = ".で並べ替え" {
        didSet {
            titleHeaderLabel.text = titleHeader + ".で並べ替え"
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    // MARK: - Initialization Method
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
