//
//  CarouselCVCell.swift
//  BEERme
//
//  Created by DU on 5/10/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class CarouselCVCell: UICollectionViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentLabel: UILabel!
    
    var beerObject: BeerObject? {
        didSet {
            guard let beerObject = beerObject else { return }
            contentLabel.text = beerObject.description!
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        // Specify you want full width
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        // Calculate the size (height) using Auto Layout
        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow)
        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
        
        // Assign the new size to the layout attributes
        autoLayoutAttributes.frame = autoLayoutFrame
        return autoLayoutAttributes
    }
}
