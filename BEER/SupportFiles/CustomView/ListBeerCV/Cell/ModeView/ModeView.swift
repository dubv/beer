//
//  ModeView.swift
//  BEERme
//
//  Created by DU on 5/10/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

protocol ModeViewChange: NSObjectProtocol {
    func modeViewChanged(mode: Int)
}

class ModeView: UIView {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var baselineVerticalSplitButton: UIButton!
    @IBOutlet private weak var baselineViewCarouselButton: UIButton!
    @IBOutlet private weak var baselineViewColumnButton: UIButton!
    
    // MARK: - Properties
    private var listModeButtons = [UIButton]()
    weak var modeViewChangeDelegate: ModeViewChange?
    
    // MARK: - Initialization Method
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    func initView() {
        Bundle.main.loadNibNamed("ModeView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.anchor(top: self.topAnchor, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: 0, left: self.leftAnchor, paddingLeft: 0, right: self.rightAnchor, paddingRight: 0, width: self.frame.width, height: self.frame.height)
    }
    
    // MARK: - Private Method
    private func setupUI() {
        listModeButtons = [baselineVerticalSplitButton, baselineViewCarouselButton, baselineViewColumnButton]
        for (button, text) in zip(listModeButtons, Constants.modeViewButtonNormal) {
            button.setImage(UIImage(named: text), for: .normal)
            button.setImage(UIImage(named: text+"_selected"), for: .selected)
        }
        listModeButtons[0].isSelected = true
    }
    
    // MARK: - Public Method
    
    // MARK: - IBAction
    @IBAction private func touchUpInsideModeChange(_ sender: UIButton) {
        //        baselineViewCarouselButton.isSelected = sender.tag == 2
        sender.isSelected = true
        let tags = Array(1...3).filter { ($0 != sender.tag) }
        modeViewChangeDelegate?.modeViewChanged(mode: sender.tag)
        
        let buttons = tags.map { self.contentView.viewWithTag($0) as? UIButton }
        buttons.forEach { $0?.isSelected = false }
    }
}
