//
//  ColumnCVCell.swift
//  BEERme
//
//  Created by DU on 5/9/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class ColumnCVCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet private weak var productImageView: UIImageView!
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productPriceLabel: UILabel!
    @IBOutlet weak var productStatusSwitch: UISwitch!
    
    // MARK: - Properties
    var beerObject: BeerObject? {
        didSet {
            setupLayout()
        }
    }

    // MARK: - Initialization Method
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Private Method
    private func setupLayout() {
        guard let dataSource = beerObject else { return }
        productDescriptionLabel.text = dataSource.description
        productNameLabel.text = dataSource.name
        productPriceLabel.text = dataSource.ingredients?.yeast
        productImageView.kf.setImage(with: URL(string: dataSource.imageURL!))
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        // Specify you want full width
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        // Calculate the size (height) using Auto Layout
        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow)
        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
        
        // Assign the new size to the layout attributes
        autoLayoutAttributes.frame = autoLayoutFrame
        return autoLayoutAttributes
    }
    
    // MARK: - Public Method
    // MARK: - Target
    
    // MARK: - IBAction
    @IBAction func productStatusAction(_ sender: Any) {
        
    }
}
