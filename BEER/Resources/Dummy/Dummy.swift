//
//  Dumy.swift
//  BEER
//
//  Created by DU on 4/24/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

struct Dummy {
    var id: Int
    var title: String
}

let dummyData = [Dummy(id: 0, title: "【API The Whole App】"), Dummy(id: 1, title: "【Search & Groupped】"), Dummy(id: 2, title: "【Countdown Timer】"), Dummy(id: 3, title: "【CollectionView Utilities】")]
