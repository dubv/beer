//
//  TicketResponse.swift
//  BEER
//
//  Created by Gone on 4/23/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class TicketResponse: Codable {
    let data: [TicketObject]?
    
    init(data: [TicketObject]?) {
        self.data = data
    }
}
