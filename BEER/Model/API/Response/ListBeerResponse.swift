//
//  ListBeerResponse.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class ListBeerResponse: Codable {
    let data: [BeerObject]?
    
    init(data: [BeerObject]?) {
        self.data = data
    }
}
