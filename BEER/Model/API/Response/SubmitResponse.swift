//
//  SubmitResponse.swift
//  BEERme
//
//  Created by DU on 5/12/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class SubmitResponse: Codable {
    let code, token, key: String?
    
    init(code: String, token: String?, key: String?) {
        self.code = code
        self.token = token
        self.key = key
    }
}
