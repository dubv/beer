//
//  SubmitRequest.swift
//  BEERme
//
//  Created by DU on 5/12/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class SubmitRequest: Codable {
    let code, security: String?
    
    init(code: String?, security: String?) {
        self.code = code
        self.security = security
    }
}
