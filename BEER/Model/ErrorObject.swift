//
//  ErrorObject.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class ErrorObject: Codable {
    let message: String?
    let errors: [DataError]?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case errors = "errors"
    }
    
    init(message: String, errors: [DataError]) {
        self.message = message
        self.errors = errors
    }
}

class DataError: Codable {
    let field: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case field = "field"
        case message = "message"
    }
    
    init(field: String, message: String) {
        self.field = field
        self.message = message
    }
}
