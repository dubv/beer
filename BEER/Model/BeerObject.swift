//
//  BeerObject.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class BeerObject: Codable {
    let id: Int?
    let name, tagline, firstBrewed, description: String?
    let imageURL: String?
    let abv: Double?
    let ibu, targetFg, targetOg, ebc: Double?
    let srm: Double?
    let ph: Double?
    let attenuationLevel: Double?
    let volume, boilVolume: BoilVolume?
    let method: Method?
    let ingredients: Ingredients?
    let foodPairing: [String]?
    let brewersTips, contributedBy: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, tagline
        case firstBrewed = "first_brewed"
        case description
        case imageURL = "image_url"
        case abv, ibu
        case targetFg = "target_fg"
        case targetOg = "target_og"
        case ebc, srm, ph
        case attenuationLevel = "attenuation_level"
        case volume
        case boilVolume = "boil_volume"
        case method, ingredients
        case foodPairing = "food_pairing"
        case brewersTips = "brewers_tips"
        case contributedBy = "contributed_by"
    }
    
    init(id: Int?, name: String?, tagline: String?, firstBrewed: String?, description: String?, imageURL: String?, abv: Double?, ibu: Double?, targetFg: Double?, targetOg: Double?, ebc: Double?, srm: Double?, ph: Double?, attenuationLevel: Double?, volume: BoilVolume?, boilVolume: BoilVolume?, method: Method?, ingredients: Ingredients?, foodPairing: [String]?, brewersTips: String?, contributedBy: String?) {
        self.id = id
        self.name = name
        self.tagline = tagline
        self.firstBrewed = firstBrewed
        self.description = description
        self.imageURL = imageURL
        self.abv = abv
        self.ibu = ibu
        self.targetFg = targetFg
        self.targetOg = targetOg
        self.ebc = ebc
        self.srm = srm
        self.ph = ph
        self.attenuationLevel = attenuationLevel
        self.volume = volume
        self.boilVolume = boilVolume
        self.method = method
        self.ingredients = ingredients
        self.foodPairing = foodPairing
        self.brewersTips = brewersTips
        self.contributedBy = contributedBy
    }
}

class BoilVolume: Codable {
    let value: Double?
    let unit: String?
    
    init(value: Double?, unit: String?) {
        self.value = value
        self.unit = unit
    }
}

class Ingredients: Codable {
    let malt: [Malt]?
    let hops: [Hop]?
    let yeast: String?
    
    init(malt: [Malt]?, hops: [Hop]?, yeast: String?) {
        self.malt = malt
        self.hops = hops
        self.yeast = yeast
    }
}

class Hop: Codable {
    let name: String?
    let amount: BoilVolume?
    let add, attribute: String?
    
    init(name: String?, amount: BoilVolume?, add: String?, attribute: String?) {
        self.name = name
        self.amount = amount
        self.add = add
        self.attribute = attribute
    }
}

class Malt: Codable {
    let name: String?
    let amount: BoilVolume?
    
    init(name: String?, amount: BoilVolume?) {
        self.name = name
        self.amount = amount
    }
}

class Method: Codable {
    let mashTemp: [MashTemp]?
    let fermentation: Fermentation?
    let twist: String?
    
    enum CodingKeys: String, CodingKey {
        case mashTemp = "mash_temp"
        case fermentation, twist
    }
    
    init(mashTemp: [MashTemp]?, fermentation: Fermentation?, twist: String?) {
        self.mashTemp = mashTemp
        self.fermentation = fermentation
        self.twist = twist
    }
}

class Fermentation: Codable {
    let temp: BoilVolume?
    
    init(temp: BoilVolume?) {
        self.temp = temp
    }
}

class MashTemp: Codable {
    let temp: BoilVolume?
    let duration: Int?
    
    init(temp: BoilVolume?, duration: Int?) {
        self.temp = temp
        self.duration = duration
    }
}
