//
//  NotificationObject.swift
//  BEER
//
//  Created by Gone on 4/23/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

typealias Notifis = [NotificationElement]

class NotificationElement: Codable {
    let id, releaseDate, title: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case releaseDate = "release_date"
        case title
    }
    
    init(id: String?, releaseDate: String?, title: String?) {
        self.id = id
        self.releaseDate = releaseDate
        self.title = title
    }
}

