//
//  TicketObject.swift
//  BEER
//
//  Created by Gone on 4/23/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class TicketObject: Codable {
    let raceID: Int?
    let nextRaceDate, nextRaceTimezone, deadlineDatetime: String?
    let active: [Active]?
    
    enum CodingKeys: String, CodingKey {
        case raceID = "race_id"
        case nextRaceDate = "next_race_date"
        case nextRaceTimezone = "next_race_timezone"
        case deadlineDatetime = "deadline_datetime"
        case active
    }
    
    init(raceID: Int?, nextRaceDate: String?, nextRaceTimezone: String?, deadlineDatetime: String?, active: [Active]?) {
        self.raceID = raceID
        self.nextRaceDate = nextRaceDate
        self.nextRaceTimezone = nextRaceTimezone
        self.deadlineDatetime = deadlineDatetime
        self.active = active
    }
}

class Active: Codable {
    let ticketID: Int?
    let type: String?
    let purchaseCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case ticketID = "ticket_id"
        case type
        case purchaseCount = "purchase_count"
    }
    
    init(ticketID: Int?, type: String?, purchaseCount: Int?) {
        self.ticketID = ticketID
        self.type = type
        self.purchaseCount = purchaseCount
    }
}
