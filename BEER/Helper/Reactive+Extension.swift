//
//  Reactive+Extension.swift
//  SI1MVVM
//
//  Created by quy nguyen on 4/16/19.
//  Copyright © 2019 SI1. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: BaseVC {
    
    internal var isAnimating: Binder<Bool> {
        return Binder(self.base, binding: { (vc, active) in
            if active {
                vc.showProgress(true)
            } else {
                vc.showProgress(false)
            }
        })
    }
}

