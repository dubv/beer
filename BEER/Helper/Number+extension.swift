//
//  Int+extension.swift
//  IDOMFleaMarket
//
//  Created by manh.nq on 1/1/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    func convertFromUnitToTenThousand() -> Double {
        return (Double(self)/10000).rounded(places: 1)
    }

    func decimal() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
    
    var stringWithLeadingZeros: String {
        return String(format: "%0d", self)
    }
}

extension Double {
    func convertTenThousandToUnit() -> Int {
        return Int(self * 10000)
    }
    
    func rounded(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension CGFloat {
    func rounded(places: Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return (self * divisor).rounded() / divisor
    }
}
