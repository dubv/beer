
//
//  Button.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 9/21/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

class ButtonObj: UIButton {
    var data : [String: Any]
    
    override init(frame: CGRect) {
        self.data = [:]
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.data = [:]
        super.init(coder: aDecoder)
    }
    
}

class UnderlineButton: UIButton {
    fileprivate var underlineColor: UIColor!
    fileprivate var underlineWidth: CGFloat!
    fileprivate var space: CGFloat!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupDrawUnderline()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupDrawUnderline()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        drawnUnderline()
    }
    
    func setupDrawUnderline(underlineColor: UIColor = .black, underlineWidth: CGFloat = 1, space: CGFloat = 1) {
        self.underlineColor = underlineColor
        self.underlineWidth = underlineWidth
        self.space = space
        self.drawnUnderline()
    }
    
    fileprivate func drawnUnderline() {
        guard let textRect = titleLabel?.frame, let decender = titleLabel?.font.descender else { return }
        let shapeLayer = CAShapeLayer()
        let path = CGMutablePath()
        path.move(to: CGPoint(x: textRect.origin.x, y: textRect.origin.y + textRect.height + decender + space))
        path.addLine(to: CGPoint(x: textRect.origin.x + textRect.width, y: textRect.origin.y + textRect.height + decender + space))
        shapeLayer.strokeColor = underlineColor.cgColor
        shapeLayer.lineWidth = underlineWidth
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
}
