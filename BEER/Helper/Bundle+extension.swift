//
//  Bundle+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 10/31/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

public let kCFBundleDisplayNameKey = "CFBundleDisplayName"
public let kCFBundleNameKey = "CFBundleName"
public let kCFBundleShortVersionKey = "CFBundleShortVersionString"

extension Bundle {
    var name: String {
        guard let info = infoDictionary else { return "" }
        return info[kCFBundleDisplayNameKey] as? String ?? info[kCFBundleNameKey] as? String ?? ""
    }
    
    var version: String {
        guard let info = infoDictionary else { return "" }
        return info[kCFBundleShortVersionKey] as? String ?? ""
    }
    
    var build: String {
        guard let info = infoDictionary else { return "" }
        return info[kCFBundleVersionKey as String] as? String ?? ""
    }
}
