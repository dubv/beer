//
//  UITextField+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 10/31/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UITextField{
    @IBInspectable
    var xibLockeyPlaceholder : String? {
        get {
            return nil
        }
        set(key){
            placeholder = key?.localized
        }
    }
    
    @IBInspectable var padding_left: CGFloat {
        get {
            return 0
        }
        set (f) {
            layer.sublayerTransform = CATransform3DMakeTranslation(f, 0, 0)
        }
    }

}
