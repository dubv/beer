//
//  Alert+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 9/17/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(_ title: String?, _ message: String?, titleButtonDone: String?, titleButtonCancel: String?, DoneAction: (() -> Void)? = nil , CancelAction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if  let titleCancel = titleButtonCancel {
            let actionCancel = UIAlertAction(title: titleCancel, style: .default) { (action: UIAlertAction) in
                
                if let CancelHandler = CancelAction {
                    CancelHandler()
                }
                
            }
            alertController.addAction(actionCancel)
        }
        
        if  let titleDone = titleButtonDone {
            let actionDone = UIAlertAction(title: titleDone, style: .default) { (action: UIAlertAction) in
                if let DoneHandler = DoneAction {
                     DoneHandler()
                }
               
            }
            alertController.addAction(actionDone)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    var GAppDelegate : AppDelegate {
        return  UIApplication.shared.delegate as! AppDelegate
    }
}
