//
//  TimerManager.swift
//  BEER
//
//  Created by Gone on 4/25/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

class TimerManager: NSObject {
    
    static var timer: Timer?
    static private var seconds = 0
    static var timerString: ((_ value: Countdown, _ ends: Bool) -> Void)?
    static var timerConvertToString: [String]?
    static var countdownTimer: Countdown?
    
    static func start(withSeconds: Int) -> Void {
        seconds = withSeconds
        timer = Timer.scheduledTimer(timeInterval: 1, target:self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    @objc static func update() -> Void {
        if seconds == 0 {
            timer?.invalidate()
            timerString?(countdownTimer!, true)
            
        } else {
            seconds -= 1
            let timer = getTimerString(from: TimeInterval(seconds))
            timerString?(timer, false)
        }
    }
    
    static private func getTimerString(from time: TimeInterval) -> Countdown {
        let firstSecondValue = Int(time) % 60 / 10
        let secondSecondValue = Int(time) % 60 % 10
        let firstMinuteValue = Int(time) / 60 % 60 / 10
        let secondMinuteValue = Int(time) / 60 % 60 % 10
        let firstHourValue = Int(time) / 3600 / 10
        let secondHourValue = Int(time) / 3600 % 10
        countdownTimer = Countdown(secondFirstNumber: firstSecondValue, secondSecondNumber: secondSecondValue, minuteFirstNumber: firstMinuteValue, minuteSecondNumber: secondMinuteValue, hourFirstNumber: firstHourValue, hourSecondNumber: secondHourValue, textColor: nil, largerFont: nil, smallFont: nil)
        return countdownTimer!
    }
    
    static func destroy() -> Void {
        timer?.invalidate()
        timer = nil
    }
    
}
