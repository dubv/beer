//
//  RxNetworkUtils.swift
//  BEERme
//
//  Created by DU on 5/11/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol RxAPIManagerProtocol : NSObjectProtocol {
    func setDefaultHeader(_ header: [String:String]?)
    func cancellAllOperation()
    func setTimeOut(_ time: TimeInterval)
}

class RxAPIManager: NSObject, RxAPIManagerProtocol {
    static let shared = RxAPIManager()
    
    //MARK: - Properties
    private var timeout : TimeInterval = 300
    private var defaultHeader : [String : String]? = [:]
    private let queue = OperationQueue()
    
    override init() {
    }
    
    //MARK: - Base Request
    public func request(url: String, method: HTTPMethod, param: Data?, header: [String:String]?, taskId: String?) -> Observable<(APIResult)> {
        return Observable.create { observer in
            
            guard let url = URL(string: url) else {
                print("Error: cannot create URL")
                let error = APIError.urlerror
                return observer.onError(error) as! Disposable
            }
            
            var urlRequest = URLRequest(url: url)
            
            urlRequest.httpMethod = method.rawValue
            
            var headers = urlRequest.allHTTPHeaderFields ?? [:]
            headers["Content-Type"] = "application/json"
            urlRequest.allHTTPHeaderFields = headers
            
            if let defaultHeader = self.defaultHeader {
                for(key, value) in defaultHeader{
                    urlRequest .setValue(value, forHTTPHeaderField: key)
                }
            }
            
            if let headers = header {
                for (key, value) in headers{
                    urlRequest .setValue(value, forHTTPHeaderField: key)
                }
            }
            
            if let bodyData = param {
                urlRequest.httpBody = bodyData
            }
            
            urlRequest.timeoutInterval = self.timeout
            
            urlRequest.cachePolicy = .reloadIgnoringCacheData
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) { (data, response, error) in
                
                guard error == nil else {
                    print("error calling API")
                    print(error!)
                    let errorServer = APIError.servererror
                    observer.onError(errorServer)
                    return
                }
                
                guard let data = data else {
                    print("Error: did not receive data")
                    let errorNoObject = APIError.objectsericalization
                    observer.onError(errorNoObject)
                    return
                }
                
                guard let response = response else {
                    observer.onError(error ?? RxCocoaError.unknown)
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    observer.onError(RxCocoaURLError.nonHTTPResponse(response: response))
                    return
                }
                
                //response code
                print("API: \(url) - http status code : \(httpResponse.statusCode)")
                
                observer.onNext(.success(httpResponse.statusCode, data))
                observer.on(.completed)
            }
            
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    //MARK: - Support Method
    public func setDefaultHeader(_ header: [String:String]?){
        defaultHeader = header;
    }
    
    public func cancellAllOperation(){
        queue.cancelAllOperations()
    }
    
    public func setTimeOut(_ time: TimeInterval){
        timeout = time
    }
}
