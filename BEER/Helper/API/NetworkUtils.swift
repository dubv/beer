//
//  Network.swift
//  SI1MVVM
//
//  Created by quy nguyen on 4/12/19.
//  Copyright © 2019 SI1. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum HTTPMethod : String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}

enum APIResult {
    case success(Int, Data)
    case failure(APIError)
}

enum APIError: Int ,Error {
    case urlerror = 400
    case unknown  = 404
    case nointernet = 401
    case objectsericalization = 405
    case parsejson = 505
    case servererror = 500
    
    var localizedDescription: String {
        switch self {
        case .urlerror:
            return TextGlobal.MESSAGE_ERROR_URL
        case .unknown:
            return TextGlobal.MESSAGE_ERROR_UNKNOWN
        case .nointernet:
            return TextGlobal.MESSAGE_NO_INTERNET
        case .objectsericalization:
            return TextGlobal.MESSAGE_ERROR_OBJECT_SERICALIZATION
        case .parsejson:
            return TextGlobal.MESSAGE_ERROR_PARSE_JSON
        case .servererror:
            return TextGlobal.MESSAGE_ERROR_CALL_SERVICE
        }
    }
    var _code : Int {
        return self.rawValue
    }
}

protocol APIManagerProtocol : NSObjectProtocol {
    func request(url: String, method: HTTPMethod , param: Data? , header: [String:String]? , taskId: String?, completion:@escaping (APIResult) -> Void)
    func setDefaultHeader(_ header: [String:String]?)
    func cancellAllOperation()
    func setTimeOut(_ time: TimeInterval)
}

class APIManager: NSObject, APIManagerProtocol {
    static let shared = APIManager()
    
    //MARK: - Properties
    private var timeout : TimeInterval = 300
    private var defaultHeader : [String : String]? = [:]
    private let queue = OperationQueue()
    
    override init() {
    }
    
    //MARK: - Base Request
    public func request(url: String, method: HTTPMethod , param: Data? , header: [String:String]? , taskId: String?, completion:@escaping (APIResult) -> Void){
        
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            let error = APIError.urlerror
            completion(.failure(error))
            return
        }
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = method.rawValue

        var headers = urlRequest.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        urlRequest.allHTTPHeaderFields = headers
        
        if let defaultHeader = defaultHeader {
            for(key, value) in defaultHeader{
                urlRequest .setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let headers = header {
            for (key, value) in headers{
                urlRequest .setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let bodyData = param {
            urlRequest.httpBody = bodyData
        }
        
        urlRequest.timeoutInterval = timeout
        
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {
                print("error calling API")
                print(error!)
                let errorServer = APIError.servererror
                completion(.failure(errorServer))
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive data")
                let errorNoObject = APIError.objectsericalization
                completion(.failure(errorNoObject))
                return
            }
            
            //response code
            let httpResponse = response as! HTTPURLResponse
            print("API: \(url) - http status code : \(httpResponse.statusCode)")
            
            completion(.success(httpResponse.statusCode, responseData))
        }
        
        if  taskId != nil {
            queue.addOperation {
                task.resume()
            }
        } else {
            task.resume()
        }
        
    }
    
    //MARK: - Support Method
    public func setDefaultHeader(_ header: [String:String]?){
        defaultHeader = header;
    }
    
    public func cancellAllOperation(){
        queue.cancelAllOperations()
    }
    
    public func setTimeOut(_ time: TimeInterval){
        timeout = time
    }
}
