//
//  Array+extension.swift
//  IDOMFleaMarket
//
//  Created by Tri iOS on 1/12/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import Foundation

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

