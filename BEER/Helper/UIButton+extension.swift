//
//  UIButton+extension.swift
//  IDOMFleaMarket
//
//  Created by quy nguyen on 10/31/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import UIKit

extension UIButton {
    @IBInspectable
    var xibLockey : String?{
        get {
            return nil
        }
        set(key){
            setTitle(key?.localized, for: .normal)
        }
    }
}

extension UIButton {
    func getAttributedStringUnderLine(foregroundColor: UIColor, font: UIFont = UIFont.defaultFont(isBold: false, fontSize: 12), title: String) -> NSMutableAttributedString {
        let underLineButtonAttributes : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor : foregroundColor,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: title, attributes: underLineButtonAttributes)
        return attributeString
    }
}
