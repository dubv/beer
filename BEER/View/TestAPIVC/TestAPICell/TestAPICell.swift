//
//  TestAPICell.swift
//  BEER
//
//  Created by Gone on 4/23/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class TestAPICell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var releaseDateLabel: UILabel!
    
    // MARK: - Properties
    public var notifi: NotificationElement? {
        didSet {
            configView()
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Initialization Method
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func configView() {
        guard let notifi = notifi else { return }
        titleLabel.text = notifi.title
        releaseDateLabel.text = notifi.releaseDate
    }
    
    // MARK: - Public Method
}
