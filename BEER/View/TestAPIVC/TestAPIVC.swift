//
//  TestAPIVC.swift
//  BEER
//
//  Created by Gone on 4/23/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TestAPIVC: BaseVC {

    // MARK: - IBOutlet
    @IBOutlet private weak var testAPITableView: UITableView!
    
    // MARK: - Properties
    private var listTicket = BehaviorRelay<[TicketObject]>(value: [])
    private var listNotifi = PublishSubject<[NotificationElement]>()
    private var baseListNotifi = PublishSubject<[NotificationElement]>()
    private var testViewModel = TestAPIViewModel()
    private var dataSource = [NotificationElement]()
    private let refreshControl = UIRefreshControl()
    private var currentPage = 1
    private var isLoadmore: Bool = false
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        callBack()
        setupBinding()
        getListNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigation()
    }
    
    //MARK: - Initialization Method
    private func setupUI() {
        
    }
    
    private func setupNavigation() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func setupBinding() {
        testAPITableView.register(TestAPICell.self)
        testAPITableView.delegate = self
        testAPITableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        baseListNotifi.asObservable().bind(to: testAPITableView.rx.items(cellIdentifier: TestAPICell.indentifier, cellType: TestAPICell.self)) { (indexPath, object, cell) in
            cell.notifi = object
        }.disposed(by: disposeBag)
    }
    
    //MARK: - Private Method
    private func getListTicket() {
        testViewModel.requestListTickets()
    }
    
    private func getListNotification() {
        isLoadmore = true
        testViewModel.requestListNotifications(pageNumber: self.currentPage, sizeNumber: 15)
    }

    private func callBack() {
        testViewModel.isLoading.bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        testViewModel.loadTicketSuccess.observeOn(MainScheduler.instance).bind(to: self.listTicket).disposed(by: disposeBag)
        testViewModel.loadNotifiSuccess.observeOn(MainScheduler.instance).bind { object in
            self.dataSource += object
            if object.isEmpty {
                self.isLoadmore = true
            } else {
                self.currentPage += 1
                self.isLoadmore = false
            }
            self.baseListNotifi.onNext(self.dataSource)
            self.refreshControl.endRefreshing()
        }.disposed(by: disposeBag)
        
        testViewModel.loadTicketFailure.observeOn(MainScheduler.instance).bind { message in
            self.showAlert(TextGlobal.MESSAGE_ERROR_PARSE_JSON, message, titleButtonDone: nil, titleButtonCancel: TextGlobal.BUTTON_OK)
        }.disposed(by: disposeBag)
    }

    // MARK: - Target
    @objc private func refreshData() {
        self.currentPage = 1
        dataSource.removeAll()
        getListNotification()
    }
}
// MARK: - UITableViewDelegate, UIScrollViewDelegate
extension TestAPIVC: UITableViewDelegate, UIScrollViewDelegate {
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == testAPITableView {
            if scrollView.contentOffset.y > scrollView.contentSize.height - scrollView.bounds.size.height - 20 && !isLoadmore {
                getListNotification()
            }
        }
    }
}
