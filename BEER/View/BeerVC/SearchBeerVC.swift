//
//  BeerVC.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchBeerVC: BaseVC {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var beerTableView: UITableView!
    
    // MARK: - Properties
    private let beerViewModel = BeerViewModel()
    private let listBeer = BehaviorRelay<[[BeerObject]]>(value: [[]])
    private let searchFilter = BehaviorRelay<[[BeerObject]]>(value: [[]])
    private var searchController = UISearchController()
    private var listBeerGrouped = [[BeerObject]]()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupBinding()
        registerCell()
        callBack()
        getListBeer()
    }
    
    // MARK: - Initialization Method
    private func setupNavigationBar() {
        self.definesPresentationContext = true
        title = "BEER me!"
        searchController = UISearchController(searchResultsController: nil)
        if #available(iOS 11, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.searchController = searchController
        }
        searchController.becomeFirstResponder()
        searchController.obscuresBackgroundDuringPresentation = false
    }
    
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    private func setupBinding() {
        beerTableView.register(HomeCell.self)
        searchController.searchBar.rx
            .text.orEmpty.observeOn(MainScheduler.instance).subscribe { query in
                self.filterBeerByQuery(query: query.element!)
        }.disposed(by: disposeBag)
    }
    
    // MARK: - Private Method
    private func registerCell() {
        beerTableView.register(HomeCell.nib, forCellReuseIdentifier: HomeCell.indentifier)
        beerTableView.delegate = self
        beerTableView.dataSource = self
    }
    
    private func callBack() {
        beerViewModel.isLoading.bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        beerViewModel.onSuccess.observeOn(MainScheduler.instance).bind(to: listBeer).disposed(by: disposeBag)
        
        listBeer.asObservable().subscribe { (beer) in
            self.listBeerGrouped = beer.element!
            self.beerTableView.reloadData()
        }.disposed(by: disposeBag)
        
        beerViewModel.onFailure.observeOn(MainScheduler.instance).bind { message in
            self.showAlert(TextGlobal.MESSAGE_ERROR_PARSE_JSON, message, titleButtonDone: TextGlobal.BUTTON_OK, titleButtonCancel: TextGlobal.BUTTON_CANCEL)
        }.disposed(by: disposeBag)
    }
    
    private func filterBeerByQuery(query: String) {
        if !query.isEmpty {
            listBeer.asObservable()
                .map {
                    $0.map { $0.filter { (($0.name?.lowercased().contains(query.lowercased()))!) } }
                }
            .bind(to: searchFilter).disposed(by: disposeBag)
            searchFilter.asObservable().subscribe { listBeer in
                self.listBeerGrouped = listBeer.element!
                self.beerTableView.reloadData()
            }.disposed(by: disposeBag)
        } else {
            listBeer.asObservable().subscribe { (beer) in
                self.listBeerGrouped = beer.element!
                self.beerTableView.reloadData()
                }.disposed(by: disposeBag)
        }
    }
    
    private func getListBeer() {
        beerViewModel.requestListBeer()
    }
    
    // MARK: - Public Method
    
    // MARK: - IBAction
}
// MARK: - UITableViewDelegate, UITableViewDataSource
extension SearchBeerVC: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listBeerGrouped[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeCell.indentifier, for: indexPath) as? HomeCell else { let cell = HomeCell(style: .default, reuseIdentifier: HomeCell.indentifier)
            return cell }
        cell.selectionStyle = .none
        cell.beer = listBeerGrouped[indexPath.section][indexPath.row]
        return cell
    }

    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listBeerGrouped.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let beerDetailVC = BeerDetailVC()
        beerDetailVC.beerData = listBeerGrouped[indexPath.section][indexPath.row]
        self.navigationController?.pushViewController(beerDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let objectSection = listBeerGrouped[section].first else { return nil }
        let label = BeerUITableViewSectionLabel()
        label.text = objectSection.name?.prefix(1).description
        let containerView = UIView()
        containerView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 10).isActive = true
        label.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20).isActive = true
        return containerView
    }
}
