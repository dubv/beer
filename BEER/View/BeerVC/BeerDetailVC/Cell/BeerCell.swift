//
//  BeerCell.swift
//  BEER
//
//  Created by Gone on 4/22/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class BeerCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var coverImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var tagLineLabel: UILabel!
    @IBOutlet private weak var desLabel: UILabel!
    @IBOutlet private weak var abvLabel: UILabel!
    @IBOutlet private weak var ibuLabel: UILabel!
    @IBOutlet private weak var targetfgLabel: UILabel!
    @IBOutlet private weak var targetogLabel: UILabel!
    @IBOutlet private weak var ebcLabel: UILabel!
    @IBOutlet private weak var srmLabel: UILabel!
    @IBOutlet private weak var phLabel: UILabel!
    @IBOutlet private weak var attenuationLevelLabel: UILabel!
    @IBOutlet private weak var volumeLabel: UILabel!
    @IBOutlet private weak var brewerTipsLabel: UILabel!
    @IBOutlet private weak var orderView: UIView!
    
    // MARK: - Properties
    public var beerObject: BeerObject? {
        didSet {
            configView()
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initialization Method
    private func configView() {
        guard let beerObject = beerObject, let imageURL = beerObject.imageURL, let volume = beerObject.volume else { return }
        
        orderView.layer.cornerRadius = 5
        
        coverImageView.kf.setImage(with: URL(string: imageURL))
        titleLabel.text = beerObject.name
        tagLineLabel.text = beerObject.tagline
        desLabel.text = beerObject.description
        abvLabel.text = "Abv: " + String(beerObject.abv!)
        ibuLabel.text = "Ibu: " + String(beerObject.ibu!)
        targetfgLabel.text = "Target FG: " + String(beerObject.targetFg!)
        targetogLabel.text = "Target OG: " + String(beerObject.targetOg!)
        ebcLabel.text = "Ebc: " + String(beerObject.ebc!)
        srmLabel.text = "Srm: " + String(beerObject.srm!)
        phLabel.text = "Ph: " + String(beerObject.ph!)
        attenuationLevelLabel.text = "Attenuation Level: " + String(beerObject.attenuationLevel!)
        volumeLabel.text = "Volume: " + String(volume.value!) + " \(volume.unit!)"
        brewerTipsLabel.text = "Brewer Tips: " + String(beerObject.brewersTips!)
    }
    
    // MARK: - Private Method
    
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
}
