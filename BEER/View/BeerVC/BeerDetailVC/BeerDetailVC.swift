//
//  BeerDetailVC.swift
//  BEER
//
//  Created by Gone on 4/19/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class BeerDetailVC: BaseVC {

    // MARK: - IBOutlet
    @IBOutlet private weak var beerDetailTableView: UITableView!
    
    // MARK: - Properties
    private var listBeer = BehaviorRelay<[BeerObject]>(value: [])
    public var beerData: BeerObject? {
        didSet {
            reloadData()
        }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigationBar()
    }

    // MARK: - Initialization Method
    private func setupNavigationBar() {
        if #available(iOS 11, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    // MARK: - Private Method
    private func reloadData() {
        guard isViewLoaded, let beerData = beerData else { return }
        listBeer.accept([beerData])
    }
    
    private func setupBinding() {
        beerDetailTableView.register(BeerCell.self)        
        listBeer.asObservable().bind(to: beerDetailTableView.rx.items(cellIdentifier: BeerCell.indentifier, cellType: BeerCell.self)) { (indexPath, object, cell) in
            cell.beerObject = object
            cell.selectionStyle = .none
            }.disposed(by: disposeBag)
        beerDetailTableView.reloadData()
    }
    
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
}
// MARK: - UITableViewDelegate
extension BeerDetailVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
}
