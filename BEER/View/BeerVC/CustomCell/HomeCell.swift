//
//  HomeCell.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import Kingfisher

class HomeCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var containBeerView: UIView!
    @IBOutlet private weak var beerImageView: UIImageView!
    
    // MARK: - Properties
    public var beer: BeerObject? {
        didSet {
            setupUI()
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        titleLabel.text = beer?.name
        beerImageView.kf.setImage(with: URL(string: beer?.imageURL ?? ""))
    }
    
    // MARK: - Public Method
}
