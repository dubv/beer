//
//  UserSubmitionVC.swift
//  BEERme
//
//  Created by DU on 5/12/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UserSubmitionVC: BaseVC {

    // MARK: - IBOutlet
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var putCodeTextView: UITextView!
    @IBOutlet private weak var securityTextView: UITextView!
    @IBOutlet private weak var submitButton: UIButton!
    
    // MARK: - Properties
    private var userSubmitionModel = UserSubmitionModel()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupBinding()
        setupCallBack()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupUI()
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        putCodeTextView.centerVertically()
        securityTextView.centerVertically()
        putCodeTextView.placeholder = putCodeTextView.text.count > 0 ? "" : "Put Code"
        securityTextView.placeholder = securityTextView.text.count > 0 ? "" : "Security"
    }
    
    // MARK: - Private Method
    private func setupBinding() {
        putCodeTextView.rx.text.orEmpty.asDriver().drive(userSubmitionModel.code).disposed(by: disposeBag)
        securityTextView.rx.text.orEmpty.asDriver().drive(userSubmitionModel.security).disposed(by: disposeBag)
        
        submitButton.rx.tap.do(onNext: {
            self.putCodeTextView.resignFirstResponder()
            self.securityTextView.resignFirstResponder()
        }).subscribe(onNext: {
            if self.userSubmitionModel.isValidate() {
                self.userSubmitionModel.requestSubmit()
            }
        }).disposed(by: disposeBag)
    }
    
    private func setupCallBack() {
        let collectionUtilitiesVC = CollectionUtilitiesVC()
        userSubmitionModel.isLoading.observeOn(MainScheduler.instance).bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        
        userSubmitionModel.onSuccess.observeOn(MainScheduler.instance).subscribe { (result) in
            self.navigationController?.pushViewController(collectionUtilitiesVC, animated: true)
            }.disposed(by: disposeBag)
        
        userSubmitionModel.onFailure.observeOn(MainScheduler.instance).subscribe { (result) in
            self.showAlert(nil, result.element, titleButtonDone: TextGlobal.BUTTON_OK, titleButtonCancel: TextGlobal.BUTTON_CANCEL)
            }.disposed(by: disposeBag)
    }
    
    // MARK: - Public Method
    // MARK: - Target
    @objc private func dismissView() {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - IBAction
}
