//
//  SelectionCVCell.swift
//  BEERme
//
//  Created by DU on 5/11/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class SelectionCVCell: UICollectionViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var mainSelectionView: UIView!
    @IBOutlet private weak var selectionLabel: UILabel!
    
    // MARK: - Properties
    var selectionTitle: String = ".で並べ替え" {
        didSet {
            selectionLabel.text = selectionTitle + ".で並べ替え"
            setupUI()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            mainSelectionView.backgroundColor = isSelected ? AppColor.selectionButtonColor : UIColor.clear
            selectionLabel.textColor = isSelected ? .white : AppColor.selectionButtonColor
        }
    }
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Initialization Method
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK: - Private Method
    private func setupUI() {
        mainSelectionView.layer.cornerRadius = mainSelectionView.frame.height/2
        mainSelectionView.layer.borderWidth = 1
        mainSelectionView.layer.borderColor = AppColor.selectionButtonColor.cgColor
    }
    
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction

}
