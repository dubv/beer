//
//  CollectionUtilitiesVC.swift
//  BEERme
//
//  Created by DU on 5/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CollectionUtilitiesVC: BaseVC {

    // MARK: - IBOutlet
    @IBOutlet private weak var selectionView: UIView!
    @IBOutlet private weak var selectionCollectionView: UICollectionView!
    @IBOutlet private weak var containCollectionView: UIView!
    
    // MARK: - Properties
    private var listBeerView = ListBeerCV()
    private let beerViewModel = BeerViewModel()
    private let listBeer = BehaviorRelay<[[BeerObject]]>(value: [[]])
    private var listBeerGrouped = [[BeerObject]]()
    private var selectionPerItem: CGFloat = 3.0
    private var sectionInsets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
    private var refreshControl = UIRefreshControl()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
        callBack()
        getListBeer()
        setupCVFlowLayout()
        registerCell()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupUI()
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        listBeerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        containCollectionView.addSubview(listBeerView)
        self.view.layoutIfNeeded()
    }
    
    private func callBack() {
        beerViewModel.isLoading.bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        beerViewModel.onSuccess.observeOn(MainScheduler.instance).bind(to: listBeer).disposed(by: disposeBag)
        
        listBeer.asObservable().subscribe { (beer) in
            self.setupData(beer: beer.element!)
            self.listBeerGrouped = beer.element!
            self.selectionCollectionView.reloadData()
            self.refreshControl.endRefreshing()
            }.disposed(by: disposeBag)
        
        beerViewModel.onFailure.observeOn(MainScheduler.instance).bind { message in
            self.showAlert(TextGlobal.MESSAGE_ERROR_PARSE_JSON, message, titleButtonDone: TextGlobal.BUTTON_OK, titleButtonCancel: TextGlobal.BUTTON_CANCEL)
            }.disposed(by: disposeBag)
    }
    
    // MARK: - Private Method
    private func setupCVFlowLayout() {
        selectionCollectionView.delegate = self
        selectionCollectionView.dataSource = self
        if let selectionCVFlowLayout = selectionCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let availabelWidth = self.selectionView.frame.width - (selectionPerItem*sectionInsets.left)
            let itemSize = availabelWidth/selectionPerItem
            selectionCVFlowLayout.itemSize = CGSize(width: itemSize, height: self.selectionView.frame.height)
        }
    }
    
    private func setupBinding() {
        listBeerView.beerCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    private func registerCell() {
        selectionCollectionView.register(SelectionCVCell.self)
    }
    
    private func getListBeer() {
        beerViewModel.requestListBeer()
    }
    
    private func setupData(beer: [[BeerObject]]) {
        self.listBeerView.dataSource = beer
    }
    
    private func setupTransformSelecteCell(indexPath: IndexPath) {
        self.selectionCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    // MARK: - Public Method
    // MARK: - Target
    @objc private func reloadData() {
        listBeerGrouped.removeAll()
        getListBeer()
    }
    // MARK: - IBAction
}
// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension CollectionUtilitiesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return listBeerGrouped.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeue(SelectionCVCell.self, forIndexPath: indexPath)
        if let beerObject = listBeerGrouped[indexPath.section].first, let selectionTitle = beerObject.name?.first {
            item.selectionTitle = selectionTitle.description
        }
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        setupData(beer: [listBeerGrouped[indexPath.section]])
        setupTransformSelecteCell(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
