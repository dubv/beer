//
//  CountdownVC.swift
//  BEER
//
//  Created by Gone on 4/25/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class CountdownVC: BaseVC {

    //MARK: - IBOutlet
    
    //MARK: - Properties
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        setupUI()
    }

    //MARK: - Initialization Method
    private func setupUI() {
    }
    
    private func setupData() {
        
    }
    
    //MARK: - Private Method
    
    //MARK: - Public Method
    func updateData() {
        
    }
    
    //MARK: - IBAction
}
