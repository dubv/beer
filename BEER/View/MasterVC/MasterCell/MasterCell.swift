//
//  MasterCell.swift
//  BEER
//
//  Created by Gone on 4/24/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

protocol MasterCellDegelate: NSObjectProtocol {
    func didSelectLikeButton(masterCell: MasterCell)
}

class MasterCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var mainTitleView: UIView!
    
    // MARK: - Properties
    
    weak var delegate: MasterCellDegelate?
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Initialization Method
    func configView(exampleType: ExampleType) {
        mainTitleView.layer.cornerRadius = 7
        titleLabel.text = exampleType.typeDefine.title
    }
    
    @IBAction private func touchUpInsideLike(_ sender: UIButton) {
        delegate?.didSelectLikeButton(masterCell: self)
    }
}
