//
//  MasterVC.swift
//  BEER
//
//  Created by Gone on 4/24/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public enum ExampleType: Int, CaseIterable {
    case testAPI = 0
    case countdown
    case testSearch
    case collectionUtilities
    case profile
    case parallel
    case animation
    
    var typeDefine: (title: String, viewController: UIViewController) {
        switch self {
        case .testAPI:
            return ("【API The Whole App】", TestAPIVC())
        case .testSearch:
            return ("【Search & Groupped】", SearchBeerVC())
        case .countdown:
            return ("【Countdown Timer】", CountdownVC())
        case .collectionUtilities:
            return ("【CollectionView Utilities】", CollectionUtilitiesVC())
        case .profile:
            return ("【Profile】", CollectionUtilitiesVC())
        case .parallel:
            return ("【CollectionViews Parallel】", CollectionParallelVC())
        case .animation:
            return ("【Animation Utils】", AnimationUtilsVC())
        }
    }
}

class MasterVC: BaseVC {

    //MARK: - IBOutlet
    @IBOutlet private weak var masterTableView: UITableView!
    
    //MARK: - Properties
    private var dataSource = PublishSubject<[ExampleType]>()
    private var researcherStateSwitch = UISwitch()
    private var viewControllerPushed = ExampleType.testAPI
    private var exampleTypes = [ExampleType]()
    private var isLoged: Bool = false
    var currentExampleTypes: [ExampleType] = []
    
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupBinding()
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupLargerNavigation()
//        currentExampleTypes.removeAll()
//        if isLoged {
//            currentExampleTypes = [.countdown,.profile, .testAPI]
//        } else {
//            currentExampleTypes = [.testAPI, .collectionUtilities]
//        }
//        masterTableView.reloadData()
    }
    
    //MARK: - Initialization Method
    private func setupLargerNavigation() {
        if #available(iOS 11, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
        }
        //私にビールを！
    }
    
    private func setupUI() {
        let testAPI = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(goTestAPI))
        let researcherStateButton = UIBarButtonItem(customView: researcherStateSwitch)
        setNavigationBar(title: nil, leftBarButton: nil, rightBarButton: [testAPI, researcherStateButton])
    }
    
    private func setupData() {
        dataSource.onNext(isLoged ? ExampleType.allCases.filter({$0 != .profile}) :  ExampleType.allCases)
    }
    
    //MARK: - Private Method
    private func setupBinding() {
        let userSubmitionVC = UserSubmitionVC()
        masterTableView.register(MasterCell.self)
        dataSource.asObserver().bind(to: masterTableView.rx.items(cellIdentifier: MasterCell.indentifier, cellType: MasterCell.self)) { (indexPath, model, cell) in
            cell.selectionStyle = .none
            cell.delegate = self
            cell.configView(exampleType: model)
            
        }.disposed(by: disposeBag)
        
        handleTableRowSelected()
        
        researcherStateSwitch.rx.isOn.changed.asObservable().subscribe { event in
            guard let event = event.element else { return }
            print("\( event == true ? "on" : "off" )")
            self.isLoged = event
            self.setupData()
            if event {
                self.navigationController?.pushViewController(userSubmitionVC, animated: true)
            } else {
                print("show logout")
            }
        }.disposed(by: disposeBag)
    }
    
    private func handlePushVC(_ viewController: ExampleType) {
        self.viewControllerPushed = viewController
        self.navigationController?.pushViewController(viewControllerPushed.typeDefine.viewController, animated: true)
    }
    
    private func handleTableRowSelected() {
        Observable.zip(masterTableView.rx.itemSelected, masterTableView.rx.modelSelected(ExampleType.self)).bind { [weak self] indexPath, model in
            guard let `self` = self, let viewController = ExampleType(rawValue: indexPath.row) else { return }
            self.handlePushVC(viewController)
        }.disposed(by: disposeBag)
    }
    
    //MARK: - Public Method
    func updateData() {
        
    }
    
    // MARK: - Target
    @objc func goTestAPI() {
        let testVC = TestAPIVC()
        navigationController?.pushViewController(testVC, animated: true)
    }
    
    //MARK: - IBAction
}
// MARK: - UITableViewDelegate
extension MasterVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

// MARK: - MasterCellDegelate
extension MasterVC: MasterCellDegelate {
    func didSelectLikeButton(masterCell: MasterCell) {
        dataSource.asObserver().subscribe { data in
            if let indexPath = self.masterTableView.indexPath(for: masterCell),
                let exampleTypes = data.element,
                exampleTypes.count > indexPath.row {
                let exampleType = exampleTypes[indexPath.row]
                print("Type: \(exampleType.typeDefine.title)")
            }
        }.disposed(by: disposeBag)
    }
}
