//
//  CollectionParallelVC.swift
//  BEERme
//
//  Created by DU on 5/14/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class CollectionParallelVC: BaseVC {

    // MARK: - IBOutlet
    @IBOutlet private weak var containCollectionView: UIView!
    
    // MARK: - Properties
    private let parallelCV = ParallelCV()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupUI()
    }

    // MARK: - Initialization Method
    private func setupUI() {
        parallelCV.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height )
        containCollectionView.addSubview(parallelCV)
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
}
