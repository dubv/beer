//
//  AnimationUtilsVC.swift
//  BEERme
//
//  Created by DU on 5/15/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class AnimationUtilsVC: BaseVC {

    // MARK: - IBOutlet
    @IBOutlet private weak var objectView: UIView!
    @IBOutlet private weak var leftObjectViewAnchorConstraint: NSLayoutConstraint!
    @IBOutlet private weak var animateHorizontalSlider: UISlider!
    
    // MARK: - Properties
    private var constant: CGFloat = 0
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        print(leftObjectViewAnchorConstraint.constant)
    }
    
    // MARK: - Private Method
    private func reloadUI(constant: CGFloat) {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.leftObjectViewAnchorConstraint.constant = constant
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Public Method
    // MARK: - Target
    
    // MARK: - IBAction
    @IBAction private func touchUpInsideBack(_ sender: UIButton) {
        self.constant -= 100
        reloadUI(constant: self.constant)
    }
    
    @IBAction private func touchUpInsideMove(_ sender: UIButton) {
        self.constant += 100
        reloadUI(constant: self.constant)
    }
    
    @IBAction func animateHorizontalSlider(_ sender: UISlider) {
        self.leftObjectViewAnchorConstraint.constant = CGFloat(sender.value)
    }
}
