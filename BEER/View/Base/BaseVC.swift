//
//  BaseVC.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit
import SVProgressHUD
import RxSwift
import RxCocoa

class BaseVC: UIViewController {
    
    //MARK: - Properties
    public let disposeBag = DisposeBag()
    var navCustomView: UIView?{
        didSet{
            self.navigationItem.titleView = navCustomView
        }
    }
    
    var navTitle: String?{
        didSet{
            let titleLabel = UILabel.init(frame: CGRect.zero)
            titleLabel.font = UIFont.defaultFont(isBold: false)
            titleLabel.textColor = AppColor.backgroundColor
            titleLabel.text = navTitle
            titleLabel.sizeToFit()
            self.navigationItem.titleView = titleLabel
        }
    }
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Initialization Method
    
    //MARK: - Private Method
    
    //MARK: - Public Method
    func setNavigationBar(title:String?, leftBarButton: [UIBarButtonItem]?, rightBarButton: [UIBarButtonItem]?){
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = AppColor.darkMainColor
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor : AppColor.darkMainColor]
        if let titleBar = title {
            self.navigationItem.title = titleBar
        }
        
        if let listLeftBarButton = leftBarButton {
            self.navigationItem.leftBarButtonItems = listLeftBarButton
        }
        
        if let listRightBarButton = rightBarButton {
            self.navigationItem.rightBarButtonItems = listRightBarButton
        }
    }
    
    func isNetworkAvailable() -> Bool {
        if Network.isConnectedToNetwork() == false {
            self.showAlert(nil, TextGlobal.MESSAGE_NO_INTERNET, titleButtonDone: TextGlobal.BUTTON_OK, titleButtonCancel: nil)
            return false
        }
        return true
    }
    
    func showProgress(_ isShow: Bool) {
        SVProgressHUD.setDefaultMaskType(.clear)
        
        if isShow {
            //show
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
        } else {
            //hide
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        }
    }
    //MARK: - Target
    
    //MARK: - IBAction
    
}
