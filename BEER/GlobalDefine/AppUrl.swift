//
//  AppUrl.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class AppUrl: NSObject {
    static let hostUrl = "https://5cb7edd21551570014da39cc.mockapi.io/"
    static let testUrl = "https://www-test.ataru.bla-one.net/"
    static let baseUrl = "api/v1/"
    
    //
    static func doSth() -> String {
        return ""
    }
    
    //Get list beers
    static func getListBeer() -> String {
        return "\(hostUrl)\(baseUrl)beers"
    }

    static func getListTicket() -> String {
        return "\(testUrl)\(baseUrl)tickets"
    }
    
    static func getListNotifi(pageNumber : Int, pageSize: Int) -> String {
        return "\(testUrl)\(baseUrl)notification?page=\(pageNumber)&size=\(pageSize)"
    }
    
    static func doSubmition() -> String {
        return "\(hostUrl)\(baseUrl)submition"
    }
}
