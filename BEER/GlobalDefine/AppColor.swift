//
//  AppColor.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

class AppColor: NSObject {
    static let backgroundColor : UIColor = UIColor(hexString: "#F5F5F5")
    static let textMainColor : UIColor = UIColor(hexString: "#333333")
    static let darkMainColor : UIColor = UIColor(hexString: "#000000")
    static let selectionButtonColor : UIColor = UIColor(hexString: "#2573AD")
    static let selectionSegmentColor : UIColor = UIColor(hexString: "#09AB8E")
}
