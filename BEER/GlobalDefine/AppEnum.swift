//
//  AppEnum.swift
//  BEER
//
//  Created by Gone on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import Foundation

enum userState {
    static var isLoged: Bool?
    static var state: Bool? {
        return isLoged
    }
}
