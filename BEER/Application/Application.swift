//
//  Application.swift
//  BEER
//
//  Created by DU on 4/18/19.
//  Copyright © 2019 BEERme. All rights reserved.
//

import UIKit

struct Application {
    static let shared = Application()
    private init() { }
    
    func configMainInterface(window: UIWindow) {
        let masterCollecVC = MasterVC()
        let nav = UINavigationController(rootViewController: masterCollecVC)
        window.rootViewController = nav
        
        window.makeKeyAndVisible()
    }
}
