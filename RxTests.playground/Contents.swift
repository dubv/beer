import UIKit
import RxSwift
import RxCocoa

// MARK: - Properties
let disposeBag = DisposeBag()
var behaviorR = BehaviorRelay<String>(value: "B0")
var publicSub = PublishSubject<String>()
var replayS = ReplaySubject<String>.create(bufferSize: 3)
var stringArray = [String]()

// MARK: - Handle

// ---------------- Behavior Replay ---------------- //
behaviorR.accept("B1")
behaviorR.accept("B1.1")

behaviorR.asObservable().subscribe { event in
    print(event.element)
}

behaviorR.accept("B2")

behaviorR.accept("B3")

// ---------------- Public Subject ---------------- //
publicSub.onNext("A1")

publicSub.asObserver().subscribe { event in
    print("PublicS -- \(event.element)")
}.disposed(by: disposeBag)

publicSub.onNext("A2")
publicSub.onNext("A3")

// ---------------- Replay Subject ---------------- //
replayS.onNext("RS1")
replayS.onNext("RS1.1")
replayS.onNext("RS1.2")
replayS.onNext("RS1.3")

replayS.asObserver().subscribe { event in
    print(event.element)
    if let value = event.element {
        stringArray.append(value)
    }
}.disposed(by: disposeBag)

stringArray.count

replayS.onNext("RS2")
replayS.onNext("RS3")
replayS.onNext("RS4")
replayS.onNext("RS5")

let shouldShowLoadingCell: Bool = false

private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
    guard shouldShowLoadingCell else { return false }
    return 4 == 4
}


