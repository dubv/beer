import UIKit

/* https://stackoverflow.com/questions/38010936/why-constant-constraints-the-property-from-a-structure-instance-but-not-the-clas */

struct A {
    var a: String
}

struct A1 {
    var a1: String
}

let a = A(a: "aaa")

//Structures in Swift are value types – and, semantically speaking, values (i.e 'instances' of value types) are immutable. ***

//a.a = "bbb" /// Không được: vì điều này nghĩa là ta chỉ định một giá trị hoàn toàn mới và nó. Nghĩa là giá trị đc thay đổi mà theo định nghĩa ngôn ngữ máy, thì instances are immutable



/// Class

// The reason you can change a mutable property of a let constant class instance, is due to the fact that classes are reference types. Therefore being a let constant only ensures that the reference stays the same. Mutating their properties doesn't in any way affect your reference to them – you're still referring to the "same" location in memory.

class B {
    var b: String = "bbb"
}

let insB = B()
insB.b = "b1b1b1b"

let arrays = ["A", "B", "C", "D", "E"]

var newArrays = [String]()

for index in 1...100 {
    arrays.forEach { value in
        newArrays.append(value)
    }
}

print(newArrays.count)
newArrays.forEach { value in
    print(value)
}
